import ROOT
import glob, os


def getFileList(path, log=False):
    if log:
        end = "reco_log.root"
    else:
        end = "reco.root"
    List = []

    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith("reco_log.root"):
                List.append(os.path.join(root, file))

    return List


modelDict = {
    "LLm": {"chirality": "LL", "interference": "const"},
    "LLp": {"chirality": "LL", "interference": "dest"},
    "LRm": {"chirality": "LR", "interference": "const"},
    "LRp": {"chirality": "LR", "interference": "dest"},
    "RLm": {"chirality": "RL", "interference": "const"},
    "RLp": {"chirality": "RL", "interference": "dest"},
    "RRm": {"chirality": "RR", "interference": "const"},
    "RRp": {"chirality": "RR", "interference": "dest"},
}

path = "/Users/Deshan/Desktop/Dilepton/scripts/output_condor/"

fileList = getFileList(path, True)

for file in fileList:
    for key in modelDict:
        if key in file:
            if "el" in file: channel = "ee"
            else: channel = "mm"
            chirality = modelDict[key]["chirality"]
            interference = modelDict[key]["interference"]
            n = "test/" + channel + "/CI_Template_" + chirality + "_log.root"
            if os.path.exists(n):
                newFile = ROOT.TFile(n, "UPDATE")
            else:
                newFile = ROOT.TFile(n, "RECREATE")
            rootFile = ROOT.TFile(file)
            if channel == "ee": nomDY = rootFile.Get("h_InvMass_DY_Pythia_el_reco_log")
            else: nomDY = rootFile.Get("h_InvMass_DY_Pythia_mu_reco_log")
            for key in rootFile.GetListOfKeys():
                h = key.ReadObj()
                name = h.GetName().split("_")
                if "CI" in name[1]:
                    h.Add(nomDY,-1)
                    h.SetName("diff_CI_" + chirality + "_" + interference + "_" + str(int(name[3]) / 1000) + "_TeV")
                    newFile.cd()
                    h.Write()
            print "Writing: ", n
            newFile.Close()

